FROM node:11-alpine

# install simple http server for serving static content
# RUN npm install -g http-server

RUN mkdir -p /client

WORKDIR /client

COPY package*.json ./

RUN npm install

COPY . .

RUN npm run build 

EXPOSE 8080

# CMD command - container executes by default when you launch the built image. 
# CMD ["http-server", "dist"]
CMD [ "npm", "start" ]